import Router from './routes';

import styles from './App.module.css';

function App() {
  return (
    <div className={styles.container}>
      <Router />
    </div>
  );
}

export default App;
