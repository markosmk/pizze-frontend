import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <div>
      <h1>Header</h1>
      {/* <Navbar /> */}
      <nav
        style={{
          borderBottom: 'solid 1px',
          paddingBottom: '1rem',
          display: 'flex',
          gap: '10px',
        }}
      >
        <Link to="/">Inicio</Link>
        <Link to="/product/name-product">Producto*</Link>
        <Link to="/category/name-category">Categoria*</Link>
        <Link to="/cart">Carrito*</Link>
        <Link to="/checkout">Checkout*</Link>
      </nav>
    </div>
  );
};

export default Header;
