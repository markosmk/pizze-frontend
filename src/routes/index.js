import { Routes, Route } from 'react-router-dom';

import PublicLayout from '../layouts/PublicLayout';
import { Home, Product, Category, Cart, Checkout, NotFound } from '../pages';

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<PublicLayout />}>
        <Route index element={<Home />} />
        <Route path="product/:slug" element={<Product />} />
        <Route path="category/:slug" element={<Category />} />
        <Route path="checkout" element={<Checkout />} />
        <Route path="cart" element={<Cart />} />
        <Route path="*" element={<NotFound />} />
      </Route>
    </Routes>
  );
};

export default Router;
