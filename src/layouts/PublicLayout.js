import { Outlet } from 'react-router-dom';
import Header from '../components/Header/Header';

const PublicLayout = () => {
  return (
    <div>
      <Header />
      <h1>Public Layout</h1>
      <Outlet />
    </div>
  );
};

export default PublicLayout;
